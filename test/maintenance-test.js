'use strict'

/* global describe, beforeEach, afterEach, it, context */

const Helper = require('hubot-test-helper')
const chai = require('chai')

const expect = chai.expect

const helper = new Helper('../test/maintenance/script.js')

describe('deprecated', () => describe('Command deprecation', () => {
  beforeEach(() => {
    this.room = helper.createRoom({
      httpd: false
    })
  })

  afterEach(() => {
    this.room.destroy()
  })

  context('A deprecated command is invoked', () => it('the bot responds in a helpful manner', (done) => {
    this.room.user.say('alice', '@hubot deprecated').then(() => {
      expect(this.room.messages).to.eql([
        ['alice', '@hubot deprecated'],
        ['hubot', '@alice I\'m sorry, but this command has been removed 😥 If this was by accident, and you were happily using it all this time, please file a bug report by messaging `@hubot bug ...description...`, or talk to your friendly infrastructure team in #ros-infra.']
      ])
    }).then(done, done)
  }))

  context('A deprecated command with a custom deprecation message is invoked', () => it('the bot responds in a helpful manner', (done) => {
    this.room.user.say('alice', '@hubot deprecated-with-custom-message').then(() => {
      expect(this.room.messages).to.eql([
        ['alice', '@hubot deprecated-with-custom-message'],
        ['hubot', '@alice A deprecated command with a custom message.']
      ])
    }).then(done, done)
  }))
}))

describe('wip', () => describe('', () => {
  beforeEach(() => {
    this.room = helper.createRoom({
      httpd: false
    })
  })

  afterEach(() => {
    this.room.destroy()
  })

  context('A command that is under construction is invoked', () => it('responds in a helpful manner when commands that are under construction are invoked', (done) => {
    this.room.user.say('alice', '@hubot wip').then(() => {
      expect(this.room.messages).to.eql([
        ['alice', '@hubot wip'],
        ['hubot', '@alice I\'m sorry, but this command is under construction 👷 You can find out more at https://example.com, or talk to your friendly infrastructure team in #ros-infra.']
      ])
    }).then(done, done)
  }))
}))

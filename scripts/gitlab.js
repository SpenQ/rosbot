'use strict'

module.exports = {
  parse_web_url_from_gitlab_output (output) {
    const lines = output.split(/\r?\n/)
    for (let line of lines) {
      if (line.indexOf('web-url') === 0) {
        const parts = line.split(':')
        parts.shift()
        return parts.join(':').trim()
      }
    }
  }
}

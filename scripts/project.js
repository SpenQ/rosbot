'use strict'

// Description:
//   Project management commands
//
// Dependencies:
//   None
//
// Configuration:
//
// Commands:
//   project status [projectName|projectType] - Show status line of project(s)
//
// Author:
//   chrismacnaughton

module.exports = robot =>
  robot.respond(/project status \s*([\s\S]*)/i, {id: 'chatops.project.status'}, function (msg) {
    if (!process.env.GITLAB_TOKEN) {
      msg.reply("Cannot talk to gitlab, don't have permission (need to set $GITLAB_TOKEN)")
      return
    }
    if (!process.env.GITWEB) {
      msg.reply("Cannot talk to gitlab, don't have gitserver name (need to set $GITWEB)")
      return
    }
    const projectName = msg.match[1].toLowerCase().trim()

    const todoListUrl = process.env.GITWEB + '/api/v4/projects/8/repository/files/To_Do_List?ref=master'

    return robot.http(todoListUrl)
      .header('Content-Type', 'application/json')
      .header('Accept', 'application/json')
      .header('Private-Token', process.env.GITLAB_TOKEN)
      .get()(function (err, res, body) {
        if (err) {
          // return msg.reply('An eror occurred: ' + err)
        }
        const data = JSON.parse(body)
        const buff = Buffer.from(data.content, 'base64')
        const text = buff.toString('ascii')
        let sent = false
        for (let s of text.split('\n')) {
          if (s === 'Processed/concluded pentests:') {
            break
          }
          if (s.toLowerCase().includes(projectName)) {
            msg.reply(s)
            sent = true
          }
        }
        if (!sent) {
          return msg.reply(`Couldn't find project ${projectName}`)
        }
      })
  })

'use strict'

const mysql = require('mysql')

module.exports = {
  gitlab_id_from_rocketchat_id (rocketchatId, cb) {
    const db = mysql.createConnection({
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: 'rosbotuser'
    })
    db.connect()
    // @todo This query needs to be updated to reflect ROS 2's new database schema, or, preferably, replaced by an API
    // call to ros-python. See https://gitlabs.radicallyopensecurity.com/ros-infra/myhubot/issues/22.
    db.query('SELECT gitlab_id FROM user WHERE rocketchat_id = ? LIMIT 1;', [rocketchatId], (error, results, fields) => {
      if (error) { throw error }
      cb(results[0].gitlab_id)
    })
    db.end()
  }
}

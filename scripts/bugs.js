'use strict'

// Description:
//   Project management commands
//
// Dependencies:
//   None
//
// Configuration:
//
// Commands:
//   bug $title $description - Files a new issue in ros-infra/roadmap with the label bug
//   feature $title $description - Files a new issue in ros-infra/roadmap with the label feature
//
// Author:
//   Chris MacNaughton

const gitlabId = require('./user').gitlab_id_from_rocketchat_id
const gitlabIssueUrl = require('./gitlab.js').parse_web_url_from_gitlab_output

let cmd = 'gitlab'

module.exports = function (robot) {
  let runCmd
  robot.respond(/bug ([\s\S]*)/i, {id: 'chatops.issue.bug'}, function (res) {
    const desc = res.match[1]
    gitlabId(res.envelope.user.id, gitlabId => {
      return createIssue(gitlabId, desc, 'Bug', res)
    })
  })

  robot.respond(/feature ([\s\S]*)/i, {id: 'chatops.issue.feature'}, function (res) {
    const desc = res.match[1]
    gitlabId(res.envelope.user.id, gitlabId => {
      return createIssue(gitlabId, desc, 'feature', res)
    })
  })

  var createIssue = function (gitlabId, description, label, res) {
    const args = ['--fancy', 'project-issue', 'create', '--project-id=617', '--title', description.slice(0, 80), '--labels', label, '--sudo', gitlabId]
    if (description.length > 79) {
      args.push('--description')
      args.push(description)
    }
    runCmd(cmd, args, function (text) {
      const issueUrl = gitlabIssueUrl(text)
      return res.send('Your issue has been created at ' + issueUrl + ' Thank you for your submission!')
    })
  }

  runCmd = function (cmd, args, cb) {
    const { spawn } = require('child_process')
    const child = spawn(cmd, args)
    child.stdout.on('data', buffer => cb(buffer.toString()))
    return child.stderr.on('data', buffer => cb(buffer.toString()))
  }
}
